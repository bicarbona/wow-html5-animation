<?php
/*
Plugin Name: Wow Html5 Animate
Version: 1.2
Description: Description
Plugin URI: https://bicarbona:lucuska@bitbucket.org/bicarbona/wow-html5-animation
Author: Author
Author URI: http://#
License: GPL2
Text Domain: wow_animate
Domain Path: Domain Path

* Bitbucket Plugin URI: https://bicarbona:lucuska@bitbucket.org/bicarbona/wow-html5-animation
* Bitbucket Branch: master

http://daneden.github.io/animate.css/
http://mynameismatthieu.com/WOW/docs.html
*/

define( 'MY_PLUGIN_VERSION', '1.0.1' );
add_action( 'wp_enqueue_scripts', 'my_plugin_enqueue' );

function my_plugin_enqueue() {
	wp_enqueue_style('animate-css', plugin_dir_url(__FILE__) . '/css/animate.min.css');
	wp_enqueue_script( 'wow-min-js', plugins_url( '/js/wow.min.js', __FILE__ ), array( 'jquery' ), MY_PLUGIN_VERSION, true );
}


//* Enqueue script to activate WOW.js
add_action('wp_enqueue_scripts', 'sk_wow_init_in_footer');
function sk_wow_init_in_footer() {
	add_action( 'print_footer_scripts', 'wow_init' );
}
 
//* Add JavaScript before </body>
function wow_init() { ?>
	<script type="text/javascript">

		wow = new WOW(
		  {
		   // boxClass:     'wow',      // default
		    //animateClass: 'animated', // default
		    offset:      100          // default
		  }
		)

	new WOW().init();
	</script>
<?php }

?>